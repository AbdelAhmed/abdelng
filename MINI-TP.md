
# Création du socle du projet

## Projet Angular

Créer le projet Angular avec routing.

Créer les modules suivants :
 - core pour le démarrage de l'application,
 - layout pour l'organisation globale des pages,
 - icons pour la gestion des icones,
 - shared (partie commune) pour faciliter les imports entre modules.


## Styles 

Ajouter ng-bootstrap en tant que dépendance.


## Icones

Importer les icones bootstrap.




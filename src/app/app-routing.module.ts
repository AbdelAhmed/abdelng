import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { PageSignInComponent } from './login/pages/page-sign-in/page-sign-in.component';
import { PageSignUpComponent } from './login/pages/page-sign-up/page-sign-up.component';
import { PageListClientsComponent } from './clients/pages/page-list-clients/page-list-clients.component';
import { PageForgotPasswordComponent } from './login/pages/page-forgot-password/page-forgot-password.component';
import { PageResetPasswordComponent } from './login/pages/page-reset-password/page-reset-password.component';

const routes: Routes = [
  {path:'', redirectTo:'/sign-in', pathMatch:'full'},
  {path:'list-clients', component: PageListClientsComponent},
  {path:'clients', loadChildren:() => import('./clients/clients.module').then(m => m.ClientsModule)},
  {path:'orders', loadChildren:() => import('./orders/orders.module').then(m => m.OrdersModule)},
  {path:'login', loadChildren:() => import('./login/login.module').then(m => m.LoginModule)},
  {path:'404-not-found', loadChildren:() => import('./page-not-found/page-not-found.module').then(m => m.PageNotFoundModule)},
 // {path:'**', redirectTo:'/404-not-found', pathMatch:'full'},
  
];

@NgModule({
  imports: [RouterModule.forRoot(
    routes,
    {
      preloadingStrategy: PreloadAllModules,
    }
    )],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { Component } from '@angular/core';
import { Observable, map } from 'rxjs';
import { VersionService } from './core/services/version.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  title: string = 'eftl-angular';
  compteur = 1;
  constructor(
    private versionService: VersionService,
  ) {}
  onNav(label: string): void {
    console.log(label);
  }
  incrementMajor(): void {
    this.versionService. incrementMajor();
  }

  incrementMinor(): void {
    this.versionService.incrementMinor();
  }

  incrementPatch(): void {
    this.versionService.incrementPatch();
  }


};





//////////////////////////////////////////////////
 // title: string = 'eftl-angular';
 // compteur = 1;
// constructor(
 // private versionService: VersionService,){ }
 // onNav(label: string) :void{
 //console.log(label) }
 // incrementVersion(): void {
   // this.versionService.incrementVersion();}
///}

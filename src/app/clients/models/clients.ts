export interface Clients{
    id: number,
    name:string,
    status:string,
    revenue:number,
    comments:string
}
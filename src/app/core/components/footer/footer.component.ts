import { Component } from '@angular/core';
import { Observable, map } from 'rxjs';
import { VersionService } from '../../services/version.service';

//import { core } from '@angular/compiler';


@Component({
  selector: 'app-footer',
  template: `version :{{version$ | async}}`,
  
})

  export class FooterComponent {
    version$: Observable<string> = this.versionService.version$.pipe(
     // (map(value => 'v' + version.major + '.' version.minor + '.' version.patch));
      map(version => `v ${version.major}.${version.minor}.${version.patch}`)
    );
  
    constructor(private versionService: VersionService) {}

//destroyRef = inject(DestroyRef);
 //version$:Observable<string> =this.versionService.version$
 
 //.pipe(map(value => 'v' + value));

 //constructor(
  //private versionService: VersionService,

 //) { }
 //ngOnInit(): void {
  // console.log("FooterComponent::ngOnInit");
  // this.versionService.version$
   //.pipe(takeUntilDestroyed(this.destroyRef))
   //.subscribe (version => {
   // console.log("FooterComponent::ngOnDestroy")
   // this.version = version
   //});
 // }
 
  //ngOnDestroy(): void {
  //console.log("FooterComponent::ngOnDestroy");
  
 }



import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconsModule } from '../icons/icons.module';
import { UiModule } from '../ui/ui.module';
import { LoginModule } from '../login/login.module';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { NavComponent } from './components/nav/nav.component';
import { Nav2Component } from './components/nav2/nav2.component';
import { Footer2Component } from './components/footer2/footer2.component';
import { Header2Component } from './components/header2/header2.component';



@NgModule({
  declarations: [
    HeaderComponent,
    NavComponent,
    Nav2Component,
    Footer2Component,
    Header2Component
  ],
  imports: [
    CommonModule
  ],
  exports: [
    IconsModule,
    UiModule,
    LoginModule,
    HeaderComponent,
    NavComponent,
    Nav2Component,
    Footer2Component,
    Header2Component
  ]
})
export class CoreModule { }


import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';




export interface Version {
  major: number;
  minor: number;
  patch: number;
};

@Injectable({
  providedIn: 'root'
})

export class VersionService {
  private version: BehaviorSubject<Version> = new BehaviorSubject<Version>({ major: 1, minor: 0, patch: 0 });

  get version$(): Observable<Version> {
    return this.version.asObservable();
  }

  constructor() { }

  incrementMajor(): void {
    //const oldVersion: Version = this.version.getValue();
   // const newVersion : Version = {
   //   major: oldVersion.major + 1,
   //   minor: oldVersion.minor,
   //   patch: oldVersion.patch
   // }

   // this.version.next(newVersion);
  //}

    const newVersion = {...this.version.getValue(), major: this.version.getValue().major + 1 };
    this.version.next(newVersion);
  }

  incrementMinor(): void {
    
    //const oldVersion: Version = this.version.getValue();
   // const newVersion : Version = {
   //   major: oldVersion.major ,
   //   minor: oldVersion.minor + 1,
   //   patch: oldVersion.patch
   // }

   // this.version.next(newVersion);

    const newVersion = {...this.version.getValue(), minor: this.version.getValue().minor + 1 };
    this.version.next(newVersion);

  }

  incrementPatch(): void {
   
    //const oldVersion: Version = this.version.getValue();
   // const newVersion : Version = {
   //   major: oldVersion.major ,
   //   minor: oldVersion.minor ,
   //   patch: oldVersion.patch + 1
   // }

   // this.version.next(newVersion);
    

    const newVersion = {...this.version.getValue(), patch: this.version.getValue().patch + 1 };
    this.version.next(newVersion);



//export class VersionService {

  //private version : BehaviorSubject<number> = new BehaviorSubject<number>(1);

 // get version$():Observable<number> {
  //  return this.version.asObservable();
  //}

 // constructor() { }
 // incrementVersion():void {
    //const Observable = this.version.getValue();
    //const newVersion = oldVersion + 1;
   // this.version.next(this.version.getValue() + 1);
    //this.version.next(newVersion);
  }

}

import { faTrash } from '@fortawesome/free-solid-svg-icons';
import { Component } from '@angular/core';

@Component({
  selector: 'app-icon-delete',
  templateUrl: './icon-delete.component.html',
  styleUrls: ['./icon-delete.component.scss', '../fa-icon.scss']
})
export class IconDeleteComponent {
  icone= faTrash;
}

import { TestBed } from '@angular/core/testing';

import { NavButtonService } from './nav-button.service';

describe('NavButtonService', () => {
  let service: NavButtonService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NavButtonService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

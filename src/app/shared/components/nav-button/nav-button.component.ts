import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-nav-button',
  template: `<button class="btn btn-outline-primary m-2" 
            [routerLinkActive]="'active'"
            [routerLink]="routes"
            (click)="sendLabel()"
            >
           {{label}}
           
   </button>`
      
})


export class NavButtonComponent {
  @Input() label!: string;
  @Input() routes: string[] = [];

  @Output() nav: EventEmitter<string> = new EventEmitter<string>();


  sendLabel():void {
   this.nav.emit(this.label);
  }

}

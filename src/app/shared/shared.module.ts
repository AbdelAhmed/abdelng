import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TemplatesModule } from '../templates/templates.module';
import { IconsModule } from '../icons/icons.module';
import { UiModule } from '../ui/ui.module';
import { NavButtonComponent } from './components/nav-button/nav-button.component';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [
    NavButtonComponent
  ],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [
    TemplatesModule,
    IconsModule,
    UiModule,
    NavButtonComponent
  ]
})
export class SharedModule { }
